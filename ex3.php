<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php

    //EXERCICE 3

    function generateArray(int $taille, int $min, int $max): array


    {
        $arr = [];
        $len = 0;
        while ($len < $taille) {
            $arr[] = rand($min, $max);
            $len += 1;
        }
        // foreach ($arr as $i) {
        //     echo "<span>$i</span>";
        // }
        return $arr;
    }

    generateArray(5, 0, 10);

    function isPrime(int $num): bool

    {
        if (!is_int($num)) {
            return false;
        }
        if ($num == 2  || $num == 0) {
            return true;
        }
        for ($i = 2; $i < abs(sqrt($num)); $i++) {
            if (($num % $i) == 0) {
                return false;
            }
        }
        return true;
    }

    $i = array_filter(generateArray(50, 0, 1000), 'isPrime');
    // foreach ($i as $it) {
    //     echo "<p>$it</p>";
    // }



    function swap(int $n1, int $n2): void
    {
        $temp = $n1;
        $n1 = $n2;
        $n2 = $temp;
    }

    function sortArr(array $arr): void
    {
        $min_idx = 0;

        for ($i = 0; $i < count($arr) - 1; $i++) {
            $min_idx = $i;
            for ($j = $i + 1; $j < count($arr); $j++)
                if ($arr[$j] < $arr[$min_idx])
                    $min_idx = $j;

            swap($arr[$min_idx], $arr[$i]);
        }
    }
    sortArr($i);
    foreach ($i as $it) {
        echo "<p>$it</p>";
    }






    ?>
</body>

</html>